import { Router } from 'express';
import { createFile, deleteFile, getAllFiles } from '../controllers/fileController';

const fileRouter = Router();

fileRouter.get('/', getAllFiles);
fileRouter.post('/', createFile);
fileRouter.delete('/:id', deleteFile);

export default fileRouter;
