import { Sequelize } from 'sequelize';

const sequelize = new Sequelize('file_uploader', 'file_uploader_user', 'password', {
  dialect: 'mysql',
  host: 'localhost',
});

export default sequelize;
