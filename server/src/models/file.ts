import { Model, DataTypes } from 'sequelize';
import sequelize from '../sequelize';

class File extends Model {
  public id!: number;
  public name!: string;
  public size!: number;
  public readonly createdAt!: Date;
  public readonly updatedAt!: Date;
}

File.init({
  id: {
    type: DataTypes.INTEGER.UNSIGNED,
    autoIncrement: true,
    primaryKey: true,
  },
  name: {
    type: DataTypes.STRING(255),
    allowNull: false,
  },
  size: {
    type: DataTypes.INTEGER.UNSIGNED,
    allowNull: false,
  },
}, {
  sequelize,
  modelName: 'File',
  tableName: 'files',
  timestamps: true,
});

export default File;
