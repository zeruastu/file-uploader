import { Request, Response } from 'express';
import { File } from '../models/index';

export const getAllFiles = async (req: Request, res: Response): Promise<void> => {
  try {
    const files = await File.findAll();
    res.status(200).json(files);
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'Internal server error' });
  }
};

export const createFile = async (req: Request, res: Response): Promise<void> => {
  try {
    const { originalname, mimetype, size } = req.file;
    const file = await File.create({ name: originalname, type: mimetype, size });
    res.status(201).json(file);
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'Internal server error' });
  }
};

export const deleteFile = async (req: Request, res: Response): Promise<void> => {
  try {
    const fileId = req.params.id;
    const file = await File.findByPk(fileId);
    if (!file) {
      res.status(404).json({ message: 'File not found' });
      return;
    }
    await file.destroy();
    res.status(200).json({ message: 'File deleted successfully' });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'Internal server error' });
  }
};
