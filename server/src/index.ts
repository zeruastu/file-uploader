import express, { Express } from 'express';
import cors from 'cors';
import fileRouter from './routers/fileRouter';
import sequelize from './sequelize';

const app: Express = express();
app.use(cors());
app.use(express.json());

app.use('/api/files', fileRouter);

sequelize
  .authenticate()
  .then(() => {
    console.log('Database connected successfully!');
    app.listen(5000, () => {
      console.log('Server started on port 5000');
    });
  })
  .catch((error) => console.log(`Error connecting to database: ${error}`));
