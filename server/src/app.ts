import express from 'express';
import cors from 'cors';
import { createServer } from 'http';
import { Server } from 'socket.io';
import sequelize from './sequelize';

const app = express();
const server = createServer(app);
const io = new Server(server, { cors: { origin: '*' } });

// Enable CORS
app.use(cors());

//
