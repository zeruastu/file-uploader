import axios from 'axios';

// Fetch all files
export const fetchFiles = async () => {
  try {
    const response = await axios.get('/api/files');
    return response.data;
  } catch (error) {
    console.error(error);
    throw new Error('Failed to fetch files!');
  }
};

// Upload a file
export const uploadFile = async (file) => {
  const formData = new FormData();
  formData.append('file', file);
  try {
    const response = await axios.post('/api/files', formData, {
      headers: {
        'Content-Type': 'multipart/form-data',
      },
    });
    return response.data;
  } catch (error) {
    console.error(error);
    throw new Error('Failed to upload file!');
  }
};

// Delete a file
export const deleteFile = async (id) => {
  try {
    const response = await axios.delete(`/api/files/${id}`);
    return response.data;
  } catch (error) {
    console.error(error);
    throw new Error('Failed to delete file!');
  }
};
