import React, { useState, useEffect } from 'react';
import { Table, Button, message } from 'antd';
import axios from 'axios';

const FileListTable = () => {
  const [files, setFiles] = useState([]);

  const fetchFiles = async () => {
    try {
      const response = await axios.get('/api/files');
      setFiles(response.data);
    } catch (error) {
      console.error(error);
      message.error('Failed to fetch files!');
    }
  };

  const deleteFile = async (id) => {
    try {
      await axios.delete(`/api/files/${id}`);
      message.success('File deleted successfully!');
      fetchFiles();
    } catch (error) {
      console.error(error);
      message.error('Failed to delete file!');
    }
  };

  const columns = [
    {
      title: 'File Name',
      dataIndex: 'name',
      key: 'name',
    },
    {
      title: 'File Size',
      dataIndex: 'size',
      key: 'size',
      render: (text) => `${text} MB`,
    },
    {
      title: 'Uploaded Date',
      dataIndex: 'createdAt',
      key: 'createdAt',
      render: (text) => new Date(text).toLocaleString(),
    },
    {
      title: 'Action',
      key: 'action',
      render: (text, record) => (
        <Button onClick={() => deleteFile(record.id)} danger>
          Remove
        </Button>
      ),
    },
  ];

  useEffect(() => {
    fetchFiles();
  }, []);

  return <Table columns={columns} dataSource={files} rowKey="id" />;
};

export default FileListTable;
