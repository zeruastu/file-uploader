import React from 'react';

interface File {
  id: number;
  fileName: string;
  fileSize: number;
  uploadedDate: Date;
}

interface Props {
  files: File[];
  onDelete: (id: number) => void;
}

const FileTable: React.FC<Props> = ({ files, onDelete }) => {
  return (
    <table>
      <thead>
        <tr>
          <th>File Name</th>
          <th>File Size</th>
          <th>Uploaded Date</th>
          <th>Action</th>
        </tr>
      </thead>
      <tbody>
        {files.map(file => (
          <tr key={file.id}>
            <td>{file.fileName}</td>
            <td>{file.fileSize} bytes</td>
            <td>{file.uploadedDate.toISOString()}</td>
            <td>
              <button onClick={() => onDelete(file.id)}>Delete</button>
            </td>
          </tr>
        ))}
      </tbody>
    </table>
  );
};

export default FileTable;
